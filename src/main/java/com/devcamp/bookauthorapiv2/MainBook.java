package com.devcamp.bookauthorapiv2;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainBook {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> bookList() {
        Author author1 = new Author("Nguyen van A", "a@gmail.com", 'm');
        Author author2 = new Author("Nguyen van B", "b@gmail.com", 'm');
        Author author3 = new Author("Nguyen van C", "c@gmail.com", 'm');
        Author author4 = new Author("Nguyen van D", "d@gmail.com", 'm');
        Author author5 = new Author("Nguyen van E", "e@gmail.com", 'f');
        Author author6 = new Author("Nguyen van F", "f@gmail.com", 'f');

        ArrayList<Author> authorsList1 = new ArrayList<>();
        ArrayList<Author> authorsList2 = new ArrayList<>();
        ArrayList<Author> authorsList3 = new ArrayList<>();
        authorsList1.add(author1);
        authorsList1.add(author2);
        authorsList2.add(author3);
        authorsList2.add(author4);
        authorsList3.add(author5);
        authorsList3.add(author6);
        System.out.println(authorsList1);

        Book book1 = new Book("Mật Mã Tài Năng", 96.5000, 9, authorsList1);
        Book book2 = new Book("Kiến Tạo Cuộc Chơi", 89.5000, 9, authorsList2);
        Book book3 = new Book("Các Cuộc Đời Ngoại Hạng", 77.5000, 91, authorsList3);
        System.out.println(book1.toString());
        ArrayList<Book> bArrayList = new ArrayList<>();
        bArrayList.add(book1);
        bArrayList.add(book2);
        bArrayList.add(book3);
        return bArrayList;

    }

}
