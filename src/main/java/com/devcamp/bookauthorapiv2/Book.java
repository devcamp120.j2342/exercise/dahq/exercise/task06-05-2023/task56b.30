package com.devcamp.bookauthorapiv2;

import java.util.ArrayList;

public class Book {
    private String name;
    private double price;
    private int qty = 0;
    private ArrayList<Author> arrayList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<Author> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<Author> arrayList) {
        this.arrayList = arrayList;
    }

    public Book(String name, double price, int qty, ArrayList<Author> arrayList) {
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.arrayList = arrayList;
    }

    @Override
    public String toString() {
        return "Book [name=" + name + ", price=" + price + ", qty=" + qty + ", arrayList=" + arrayList + "]";
    }

}
